FROM golang:latest AS hugo-builder
ARG HUGO_VERSION=0.145.0
RUN apt-get update && \
    apt-get install -y git build-essential && \
    CGO_ENABLED=1 go install --tags extended github.com/gohugoio/hugo@v${HUGO_VERSION}

FROM buildpack-deps:bookworm AS node-builder
ARG NODE_VERSION=22.14.0
WORKDIR "/opt"
RUN set -ex \
  # gpg keys listed at https://github.com/nodejs/node#release-keys
  && for key in \
    C0D6248439F1D5604AAFFB4021D900FFDB233756 \
    4ED778F539E3634C779C87C6D7062848A1AB005C \
    141F07595B7B3FFE74309A937405533BE57C7D57 \
    74F12602B6F1C4E913FAA37AD3A89613643B6201 \
    DD792F5973C6DE52C432CBDAC77ABFA00DDBF2B7 \
    CC68F5A3106FF448322E48ED27F5E38D5B0A215F \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
    890C08DB8579162FEE0DF9DB8BEAB4DFCF555EF4 \
    C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C \
    108F52B48DB57BB0CC439B2997B01419BD92F80A \
    A363A499291CBBC940DD62E41F10027AF002F8B0 \
  ; do \
      gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz" \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && grep " node-v$NODE_VERSION-linux-x64.tar.gz\$" SHASUMS256.txt | sha256sum -c - \
  && mv "node-v$NODE_VERSION-linux-x64.tar.gz" "node.tar.gz"

# Second stage - build the final image with minimal dependencies.
FROM golang:latest
COPY --from=node-builder /opt/node.tar.gz /opt
RUN tar -xf "/opt/node.tar.gz" -C /usr/local --strip-components=1 --no-same-owner \
  && rm "/opt/node.tar.gz" \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs

COPY --from=hugo-builder /go/bin/hugo /usr/bin
# libc6-compat & libstdc++ are required for extended SASS libraries
# ca-certificates are required to fetch outside resources (like Twitter oEmbeds)
# rsync, git, openssh-client for deployment
RUN apt-get update && \
    apt-get install -y ca-certificates git libc6 libstdc++6 tzdata rsync git openssh-client && \
    # tests
    hugo version && \
    node --version && \
    npm --version