#!/usr/bin/env python3
"""
Check Hugo new releases from hugo and nodejs GitHub repos and update image automatically

Usage: update.py
"""
import os
import re

import requests
from git import Repo, Commit
from packaging import version
from packaging.version import Version


def update_hugo_version() -> (bool, Version):
    with open('Dockerfile', 'rt') as infile:
        dockerfile = infile.read()

    hugo_regex = r'(ARG HUGO_VERSION=)(\d+\.\d+\.\d+)'
    hugo_current_version = version.parse(re.search(hugo_regex, dockerfile, flags=re.MULTILINE).group(2))
    print(f'Current hugo version is {hugo_current_version}')

    hugo_latest_release_json = requests.get('https://api.github.com/repos/gohugoio/hugo/releases/latest').json()
    hugo_latest_version = version.parse(hugo_latest_release_json['tag_name'])
    print(f'Lastest hugo version is {hugo_latest_version}')

    if hugo_current_version >= hugo_latest_version:
        print('Already up to date, nothing to do')
        return False, hugo_current_version

    print(f'Version {hugo_latest_version} > {hugo_current_version}. Updating the hugo version.')

    with open('Dockerfile', 'wt') as outfile:
        outfile.write(re.sub(hugo_regex, r'\g<1>{}'.format(hugo_latest_version), dockerfile, re.MULTILINE))
    return True, hugo_latest_version


def update_node_version() -> (bool, Version):
    with open('Dockerfile', 'rt') as infile:
        dockerfile = infile.read()

    node_regex = r'(ARG NODE_VERSION=)(\d+\.\d+\.\d+)'
    node_current_version = version.parse(re.search(node_regex, dockerfile, flags=re.MULTILINE).group(2))
    print(f'Current node version is {node_current_version}')

    node_releases = requests.get('https://api.github.com/repos/nodejs/node/releases').json()
    # we expect the releases in order from newest to oldest. if this is not the case this needs to be changed.
    for release in node_releases:
        if release['prerelease'] or release['draft']:
            continue
        release_version = version.parse(release['tag_name'])
        if release_version.major != node_current_version.major:  # we only want to update within one major version automatically
            continue

        print(f'Lastest node {node_current_version.major}.x.x version is {release_version}')
        if node_current_version >= release_version:
            print('Already up to date, nothing to do')
            return False, node_current_version

        print(f'Version {release_version} > {node_current_version}. Updating the node version.')

        with open('Dockerfile', 'wt') as outfile:
            outfile.write(re.sub(node_regex, r'\g<1>{}'.format(release_version), dockerfile, re.MULTILINE))
        return True, release_version


def commit_changes(git_repo: Repo, package_name: str, package_version: Version) -> Commit:
    index = git_repo.index
    index.add(['Dockerfile'])

    print(f'Commited update of {package_name} to {package_version}')
    return index.commit(f'upd: {package_name} {package_version}')


if __name__ == '__main__':
    env = os.environ

    gitlab_url = env['CI_SERVER_URL']
    gitlab_token = env['CI_JOB_TOKEN']
    gitlab_project_id = env['CI_PROJECT_ID']
    gitlab_branch = env['CI_DEFAULT_BRANCH']

    repo = Repo('.')

    updated_hugo, hugo_version = update_hugo_version()
    if updated_hugo:
        commit = commit_changes(repo, 'hugo', hugo_version)

    updated_node, node_version = update_node_version()
    if updated_node:
        commit = commit_changes(repo, 'node', node_version)

    if updated_hugo or updated_node:
        tag_name = f'hugo-{hugo_version}_node-{node_version}'
        repo.create_tag(
            tag_name,
            ref=commit.hexsha,
            message='Auto-created tag by CI-Bot'
        )
        print(f'Tag {tag_name} created')

    print('Done!')
